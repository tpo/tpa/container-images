# based on the upstream Fedora-based podman image
# https://github.com/containers/image_build/blob/main/podman/Containerfile

ARG SUITE=bookworm

FROM containers.torproject.org/tpo/tpa/base-images/debian:${SUITE}

RUN apt-get update -q && \
  apt-get install -yq --no-install-recommends \
    arch-test \
    containers-storage \
    curl \
    dctrl-tools \
    distro-info \
    jq \
    mmdebstrap \
    libcap2-bin \
    mtree-netbsd \
    podman \
    qemu-user-static \
    skopeo \
    uidmap && \
  ( apt-get install -yq ubuntu-keyring || true ) && \
  apt-get clean && \
  rm -rf /var/lib/apt/lists/*

RUN useradd podman && chage --lastday -1 podman && rm -f /etc/shadow- && \
    echo "podman:1:999\npodman:1001:64535" > /etc/subuid && \
    echo "podman:1:999\npodman:1001:64535" > /etc/subgid

ADD /containers.conf /etc/containers/containers.conf
ADD /podman-containers.conf /home/podman/.config/containers/containers.conf

RUN mkdir -p /home/podman/.local/share/containers && \
    chown podman:podman -R /home/podman && \
    chmod 644 /etc/containers/containers.conf \
      /home/podman/.config/containers/containers.conf

# Note VOLUME options must always happen after the chown call above
# RUN commands can not modify existing volumes
VOLUME /home/podman/.local/share/containers

# replace setuid bits with capabilities
RUN chmod u-s /usr/bin/new[gu]idmap && \
  setcap cap_setuid=ep /usr/bin/newuidmap && \
  setcap cap_setgid=ep /usr/bin/newgidmap

ENV _CONTAINERS_USERNS_CONFIGURED="" \
    BUILDAH_ISOLATION=chroot

USER podman
