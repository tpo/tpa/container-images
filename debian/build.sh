#!/bin/bash

# XXX: this should be re-written to be part of our installer, possibly
# as a submodule of tsa-misc, but maybe not in fabric

set -u

# The mmdebstrap(1) script will automatically add the typical debian mirrors,
# but if you wish to add an additional mirror (such as backports), you can pass
# the environment variable ADD_MIRROR to add a debian mirror to
# /etc/apt/sources.list.d/custom.list.
#
# example in .gitlab-ci.yml:
#
# bullseye-backports:
#  stage: build
#  variables:
#    SUITE: bullseye
#    ADD_MIRROR: "deb http://deb.debian.org/debian bullseye-backports main"
#  <<: *build_debian_image
#
# bash-specific
set -o pipefail

if [ -z "$IMAGE" ]; then
    echo "Unable to build an image without \$IMAGE!"
    exit 1
elif [ -z "$SUITE" ]; then
    echo "Unable to build an image without \$SUITE!"
    exit 1
fi

NEED_PUSH=0
ARCHITECTURES="amd64 arm64"

DEBIAN_MIRROR="http://deb.debian.org/debian"
UBUNTU_MIRROR="http://archive.ubuntu.com/ubuntu"
UBUNTU_PORTS_MIRROR="http://ports.ubuntu.com/ubuntu-ports"
OLD_UBUNTU_MIRROR="http://old-releases.ubuntu.com/ubuntu"

# set proper mirror url for epoch and special cases
if [ "$IMAGE" = "debian" ]; then
    MIRROR=$DEBIAN_MIRROR
    if echo "$SUITE" | grep -q -- '-backports$'; then
        TAG="$SUITE"
        SUITE=$(echo "$SUITE" | cut -d- -f1)
        ADD_MIRROR="deb ${MIRROR} ${SUITE}-backports main"
    fi
elif [ "$IMAGE" = "ubuntu" ]; then
    if curl -s -f -LI "${UBUNTU_MIRROR}/dists/${SUITE}/" > /dev/null; then
        echo "I: Found '${SUITE}' on Ubuntu repositories, building an Ubuntu image."
        COMPONENTS="main,universe"
        MIRROR=$UBUNTU_MIRROR
        if [ "$SUITE" = "focal" ]; then
            # workaround for dash package installation failure on focal
            EXTRA_MMDEBSTRAP_OPT=--dpkgopt='path-include=/usr/share/man/man1/sh.1.gz'
        fi
    elif curl -s -f -LI "${OLD_UBUNTU_MIRROR}/dists/${SUITE}/" > /dev/null; then
        echo "I: Found '${SUITE}' on Ubuntu repositories, building an Ubuntu image."
        COMPONENTS="main,universe"
        MIRROR=$OLD_UBUNTU_MIRROR
        MMDEBSTRAP_MIRROR=$OLD_UBUNTU_MIRROR
    else
        echo "Unable to find an Ubuntu mirror for '${SUITE}'!"
        exit 1
    fi
else
    echo "We don't know how to build '${IMAGE}'!"
    exit 1
fi

MODE="auto"
VARIANT="minbase"
IMAGE_TAG="$IMAGE:${TAG:-$SUITE}"

# create a manifest list for multi-arch
podman manifest create "localhost/$IMAGE_TAG" > /dev/null

# shellcheck disable=SC2016
for arch in $ARCHITECTURES; do
    echo -e "\e[33mI: starting build for '${IMAGE_TAG}' image with architecture '${arch}'...\e[0m"

    if [ "$MIRROR" = "$UBUNTU_MIRROR" ] && [ "$arch" != "amd64" ]; then
        MIRROR="$UBUNTU_PORTS_MIRROR"
    fi

    # extract mirror Release date to generate SOURCE_DATE_EPOCH
    # to assist with image reproducibility
    MIRROR_DATE=$(curl -s "${MIRROR}"/dists/"${SUITE}"/Release | /usr/bin/grep-dctrl -s Date -n '')
    echo -e "\e[33mI: mirror $MIRROR timestamp is '$MIRROR_DATE'\e[0m" >&2
    SOURCE_DATE_EPOCH=$(date --date="$MIRROR_DATE" +%s)
    export SOURCE_DATE_EPOCH
    echo -e "\e[33mI: epoch is $SOURCE_DATE_EPOCH seconds\e[0m" >&2

    # this creates an image similar to the official ones built by
    # debuerreotype, except that instead of needing a whole set of scripts
    # and hacks, we only rely on mmdebstrap.
    #
    # the downside is that the image is not reproducible (even if ran
    # against the same mirror without change), mainly because `docker
    # import` stores the import timestamp inside the image. however, the
    # internal checksum itself should be reproducible.
    if ! mmdebstrap \
      --mode=$MODE \
      --variant=$VARIANT \
      --aptopt='Acquire::Languages "none"' \
      --dpkgopt='path-exclude=/usr/share/man/*' \
      --dpkgopt='path-exclude=/usr/share/man/man[1-9]/*' \
      --dpkgopt='path-exclude=/usr/share/locale/*' \
      --dpkgopt='path-include=/usr/share/locale/locale.alias' \
      --dpkgopt='path-exclude=/usr/share/lintian/*' \
      --dpkgopt='path-exclude=/usr/share/info/*' \
      --dpkgopt='path-exclude=/usr/share/doc/*' \
      --dpkgopt='path-include=/usr/share/doc/*/copyright' \
      --dpkgopt='path-exclude=/usr/share/omf/*' \
      --dpkgopt='path-exclude=/usr/share/help/*' \
      --dpkgopt='path-exclude=/usr/share/gnome/*' \
      --dpkgopt='path-exclude=/usr/share/examples/*' \
      --include='ca-certificates' \
      --setup-hook='cp minimize-config/dpkg.cfg.d/* "$1/etc/dpkg/dpkg.cfg.d/"' \
      --setup-hook='cp minimize-config/apt.conf.d/* "$1/etc/apt/apt.conf.d/"' \
      --components="${COMPONENTS:=main}" \
      ${ADD_MIRROR:+--setup-hook='echo "'"$ADD_MIRROR"'" >  "$1"/etc/apt/sources.list.d/custom.list'} \
      --customize-hook='rm "$1"/etc/resolv.conf' \
      --customize-hook='rm "$1"/etc/hostname' \
      --architectures="${arch}" \
      ${EXTRA_MMDEBSTRAP_OPT:=} \
      "$SUITE" - ${MMDEBSTRAP_MIRROR:=} |  podman import --arch "$arch" -c 'CMD ["bash"]' - "localhost/${arch}/$IMAGE_TAG"
       then
           echo "E: failed to build new image" >&2
           exit 1
    fi

    # add the resulting image to the multi-arch manifest list
    podman manifest add "localhost/$IMAGE_TAG" "containers-storage:localhost/${arch}/$IMAGE_TAG"
done

# Determine if this newly built image is different from the one in the registry.
# If it is different, then something has updated, and we want the newer version in the registry.
# If it is not different, then we do not want to push this newer version. The
# reason why we don't want to push the newer version is because the container
# digest changes each time, even if the contents are the same. This is due to
# some timestamps that `podman import` puts in there, and no amount of
# libfaketime has been able to change that. (see
# https://github.com/containers/podman/issues/14978#issuecomment-1750655731)

# Get digest of the top layer of the RootFS of the new image and compare it to
# the existing image in the registry. The image built from mmdebstrap only has
# one layer, but this allows us to pull the value from the array

# This is the image name on the registry.
registry_dest="$CI_REGISTRY_IMAGE/$IMAGE_TAG"
registry_dest_date="${CI_REGISTRY_IMAGE}/$IMAGE_TAG-$(date '+%Y%m%d')"

for arch in $ARCHITECTURES; do
    remote_image_digest=$(skopeo inspect --raw "docker://${registry_dest}" | jq -r ".manifests[] | select(.platform.architecture==\"${arch}\") | .digest")
    if [ -z "$remote_image_digest" ]; then
        echo -e "\e[33mI: Image ${IMAGE_TAG} with architecture ${arch} not found in registry, push needed!\e[0m"
        NEED_PUSH=1
        break
    fi

    remote_rootfs_digest=$(skopeo inspect --config --raw "docker://${CI_REGISTRY_IMAGE}/${IMAGE}@${remote_image_digest}" | jq -r '.rootfs.diff_ids[0]')
    local_rootfs_digest=$(podman inspect --type image --format "{{index .RootFS.Layers 0}}" "localhost/${arch}/${IMAGE_TAG}")

    if [ "${local_rootfs_digest}" != "${remote_rootfs_digest}" ]; then
        echo -e "\e[33mI: ${arch} registry/local rootfs digests differ: ${remote_rootfs_digest} / ${local_rootfs_digest}\e[0m"
        NEED_PUSH=1
        break
    else
        echo -e "\e[33mI: ${arch} registry/local rootfs digests match: ${remote_rootfs_digest}\e[0m"
    fi
done

if [ $NEED_PUSH != 1 ]; then
    echo -e "\e[33mI: rootfs digests for all architectures match, not pushing to registry.\e[0m"
    exit 0
else
    echo -e "\e[33mI: rootfs digests mismatch for one or more architectures, registry push needed.\e[0m"
fi

if [ "$CI_PROJECT_PATH" = "tpo/tpa/base-images" ] && [ "$CI_COMMIT_BRANCH" != "$CI_DEFAULT_BRANCH" ]; then
    echo -e "\e[33mI: Not on default branch, pushing images to container registry will abort now.\e[0m"
    exit 0
fi

push_image()
{
    dest="$1"
    echo -e "\e[33mI: Pushing new image to registry to $dest\e[0m"
    if ! podman manifest push "localhost/$IMAGE_TAG" "$dest"; then
        echo "E: failed to push image" >&2
        exit 1
    fi
}

podman login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"

for dest in $registry_dest $registry_dest_date; do
    push_image "$dest"
done

if [ "$IMAGE" = "debian" ]; then
    alias=$(debian-distro-info --alias="$SUITE")
    case "$registry_dest" in
        *"-backports")
            if [ "$alias" == "stable" ]; then
                push_image "$CI_REGISTRY_IMAGE/$IMAGE:stable-backports"
            fi
            ;;
        *)
            # push an stable/testing/unstable alias as well
            # $alias will be the same as $suite if not stable/testing/unstable, so ignore that
            if [ "$alias" != "$SUITE" ]; then
                push_image "$CI_REGISTRY_IMAGE/$IMAGE:$alias"
            fi
            ;;
    esac
elif [ "$IMAGE" = "ubuntu" ]; then
    devel=$(ubuntu-distro-info --devel)
    [ "$SUITE" = "$devel" ] && push_image "$CI_REGISTRY_IMAGE/$IMAGE:devel"
    stable=$(ubuntu-distro-info --stable)
    [ "$SUITE" = "$stable" ] && push_image "$CI_REGISTRY_IMAGE/$IMAGE:stable"
    lts=$(ubuntu-distro-info --lts)
    [ "$SUITE" = "$lts" ] && push_image "$CI_REGISTRY_IMAGE/$IMAGE:lts"
fi

touch /tmp/.tag_cleanup

# note that we could also have used `| tar --delete /$PATH` to exclude files
