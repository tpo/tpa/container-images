# Overview

The base-images project provides a set of foundational container images for use
in CI/CD pipelines, eliminating reliance on external registries like
DockerHub. These images serve as minimal, bootstrapped environments tailored for
commonly used software stacks, ensuring consistency, reliability, and control
over dependencies.

A base image is a container image that has the base minimum pieces necessary to
build other containers.

## Key Features:

- **Minimal Base Images** – Provides multi-arch, minimal Debian and Ubuntu
  images, including only what is necessary to bootstrap a functional system.

- **Layered Approach** – Additional images, such as a Python development image,
  extend the Debian base with essential Python utilities for building projects
  in CI, with a minimal number of intermediate images.

- **Self-Hosted Registry** – All images are built and maintained in this
  project, hosted in this project's container registry to avoid DockerHub rate
  limits and dependency on third-party sources.

- **Root of Trust** – Ensures base images are built directly from Debian’s trust
  anchors, improving security and maintainability.

- **Expandable Ecosystem** – Teams are welcome to collaborate to
  contribute and maintain other frequently used base images, possibly
  shared across teams, streamlining CI builds and development workflow

## Why This Matters:

- **Avoid CI Failures** - Eliminates reliance on DockerHub, preventing pipeline
  disruptions due to rate limiting.

- **Security & Trust** - Reduces exposure to externally managed images, ensuring
  provenance from trusted sources.

- **Performance Gains** - Faster and more reliable builds by leveraging
  pre-cached internal images instead of pulling from external registries.

Teams can integrate these images into their pipelines, benefiting from a stable and self-sustained container ecosystem tailored to our development needs.

## Using the images

### GitLab CI

To use these images to run GitLab CI jobs, use the image's full URL:

    containers.torproject.org/tpo/tpa/base-images/<imagename>:<tag>

For example, to use the Debian stable image:

    containers.torproject.org/tpo/tpa/base-images/debian:stable

To use derivative images such as the Python base image, the Debian release
codename must be used, eg:

    containers.torproject.org/tpo/tpa/base-images/python:bookworm

This is required because the Python interpreter is updated between different
Debian releases, and compatibility can't be guaranteed.

### Install packages with apt-get

It's possible to install additional packages in the containers when running in
GitLab CI, however some initialisation is needed. TPA provides a CI template to
help with this:

```
include:
  - project: tpo/tpa/ci-templates
    file: scripts/apt.yml
```

Then, in your job's `before_script` key, add `- !reference [.apt-init]`

## Contributing

Contributions are welcome! If your team would benefit from additional base
images, you can either submit a MR here, or propose a new image and we can help
you.

## Build process

### Debian base image

The `debian/build.sh` handles the work of creating, tagging, and pushing the
base Debian images.

These images are built "from scratch" using `mmdebstrap` which creates a
functional system directly from the Debian archive. Some customization and
cleanup is applied to minimize the footprint of the resulting image.

To ensure images are always fresh, they are rebuilt every day from the upstream
archive. If the resulting root filesystem is different, the image is tagged and
uploaded to the registry.  If not, the image is not pushed.

### Derivative images

This project also builds several images with a few extras on top of the base
Debian system:

  * `golang`: basic Golang build environment, based on Debian's
    [golang package](http://tracker.debian.org/golang)
  * `podman`: rootless podman installation useful for building containers
  * `python`: Python interpreter with the pip and venv modules
  * `redis-server`: Redis instance listening on default port 6379

Like the base images, the derivative images are also rebuilt every day to
account for updates to the base image and the extra packages. The contents of
the resulting images are compared using the [mtree tool][] to the ones
currently in the registry, and the new image is only pushed when changes are
found.

[mtree tool]: https://packages.debian.org/stable/mtree-netbsd

# About the project avatar

The avatar of this GitLab project refers to the [All your base are
belong to us](https://en.wikipedia.org/wiki/All_your_base_are_belong_to_us) meme that refers to a game from the previous
millenia, [Zero Wing (1991)](https://en.wikipedia.org/wiki/Zero_Wing). It refers to the tendency of TPA to
try to make people use our image to help with supply chain
security. Any reference to actual world domination is pure
coincidence, you're (currently) free to use the images of your
choosing.

It is considered to be fair use here.
